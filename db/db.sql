CREATE TABLE movie (
    movie_id INTEGER PRIMARY KEY auto_increment,
    movie_title VARCHAR(25),
    movie_release_date DATE,
    movie_time TIME,
    director_name VARCHAR(25)
);


INSERT INTO movie (movie_title, movie_release_date, movie_time, director_name) VALUES ('3 Idiots', '2008-12-05' , '02:55:45', 'Rajkumar Hirani');
INSERT INTO movie (movie_title, movie_release_date, movie_time, director_name) VALUES ('Avengers', '2012-05-19' , '02:36:58', 'Joe Russo');
INSERT INTO movie (movie_title, movie_release_date, movie_time, director_name) VALUES ('Eternals', '2021-07-25' , '02:49:16','Chloe Zhao');